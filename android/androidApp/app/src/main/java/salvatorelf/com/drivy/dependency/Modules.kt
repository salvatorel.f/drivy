package salvatorelf.com.drivy.dependency

import org.koin.core.module.Module
import org.koin.dsl.module
import salvatorelf.com.drivy.network.NetworkClient


class Modules {
    private var modules = ArrayList<Module>()

    private val networkModule = module {
        single { NetworkClient.create() }
        modules.add(this)
    }

    fun getModules(): MutableList<Module> {
        return modules
    }
}