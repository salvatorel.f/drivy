package salvatorelf.com.drivy.view.detail

import android.os.Bundle
import android.view.MenuItem
import androidx.databinding.DataBindingUtil
import salvatorelf.com.drivy.R
import salvatorelf.com.drivy.databinding.ActivityDetailBinding
import salvatorelf.com.drivy.model.Car
import salvatorelf.com.drivy.view.BaseActivity

class DetailActivity : BaseActivity() {
    companion object {
        const val CAR = "car"
    }

    lateinit var binding: ActivityDetailBinding
    lateinit var car: Car

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_detail)
        car = intent.getParcelableExtra(CAR)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setTitle(car.brand.plus(getString(R.string.space)).plus(car.model))
        binding.car = car
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.getItemId()) {
            android.R.id.home -> {
                super.onBackPressed()
                return true
            }

            else -> return super.onOptionsItemSelected(item)
        }
    }
}
