package salvatorelf.com.drivy.view.main

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import salvatorelf.com.drivy.R
import salvatorelf.com.drivy.databinding.CarItemBinding
import salvatorelf.com.drivy.model.Car

interface CarItemCallback {
    fun onCarClicked(car: Car)

}

class CarsAdapter(val carItemCallback: CarItemCallback) : RecyclerView.Adapter<CarsAdapter.CarHolder>(),
    CarItemCallback {


    lateinit var binding: CarItemBinding
    var cars: List<Car> = emptyList()
    lateinit var carHolder: CarHolder

    fun setData(cars: List<Car>) {
        this.cars = cars
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CarHolder {
        val inflater = LayoutInflater.from(parent.context)
        binding = DataBindingUtil.inflate(inflater, R.layout.car_item, parent, false)
        //I decided to do not create a viewModel for a single click. To show that i can use different patterns.
        binding.onClickListener = this
        carHolder = CarHolder(binding)
        return carHolder
    }

    override fun getItemCount(): Int {
        return cars.size
    }

    override fun onBindViewHolder(holder: CarHolder, position: Int) {
        holder.bind(cars.get(position))
    }

    class CarHolder(val binding: CarItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(car: Car) {
            binding.car = car
        }
    }


    override fun onCarClicked(car: Car) {
        carItemCallback.onCarClicked(car)
    }
}