package salvatorelf.com.drivy.view.main

import androidx.databinding.ObservableBoolean
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import salvatorelf.com.drivy.model.Car
import salvatorelf.com.drivy.model.CarsWrapper
import salvatorelf.com.drivy.network.NetworkService

class MainActivityViewModel(val networkService: NetworkService) : ViewModel() {
    val isLoading = ObservableBoolean(false)
    val cars = MutableLiveData<CarsWrapper>()
    val errorVisibility = ObservableBoolean(false)

    fun getCars() {
        isLoading.set(true)
        networkService.getCars().enqueue(object : Callback<List<Car>> {
            override fun onResponse(call: Call<List<Car>>, response: Response<List<Car>>) {
                isLoading.set(false)
                response.body()?.let {
                    errorVisibility.set(false)
                    cars.value = CarsWrapper(it)
                }
            }

            override fun onFailure(call: Call<List<Car>>, t: Throwable) {
                isLoading.set(false)
                if (cars.value == null) {
                    errorVisibility.set(true)
                }
                cars.value = CarsWrapper(t)
            }
        })
    }

    fun onRefresh() {
        isLoading.set(true)
        getCars()
    }

}