package salvatorelf.com.drivy.model

import android.annotation.SuppressLint
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
@SuppressLint("ParcelCreator")
//Bug on parcelize https://youtrack.jetbrains.com/issue/KT-19300
data class Rating(val average: Double? = 0.0, val count: Int? = 0) : Parcelable