package salvatorelf.com.drivy.network

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers
import salvatorelf.com.drivy.model.Car

interface NetworkService {
    @GET("cars.json")
    @Headers("Content-Type: application/json")
    fun getCars(): Call<List<Car>>
}