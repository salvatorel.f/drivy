package salvatorelf.com.drivy.model

import android.annotation.SuppressLint
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import salvatorelf.com.drivy.BuildConfig
import java.text.NumberFormat
import java.util.*

@Parcelize
@SuppressLint("ParcelCreator")
//Bug on parcelize https://youtrack.jetbrains.com/issue/KT-19300
data class Car(
    val brand: String? = "",
    val model: String? = "",
    @SerializedName("picture_url") val pictureUrl: String? = "",
    @SerializedName("price_per_day") val pricePerDay: Int? = 0,
    val rating: Rating,
    val owner: Owner
) : Parcelable {

    fun getPriceCurrency(): String {
        val format = NumberFormat.getCurrencyInstance()
        format.maximumFractionDigits = 0
        format.currency = Currency.getInstance(BuildConfig.CURRENCY_CODE)

        return format.format(pricePerDay)
    }
}